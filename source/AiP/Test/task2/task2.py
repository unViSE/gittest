import time


el = 0
data = []

# a loop for reading from a file to the data array
while True:
    try:
        el = input().strip('\n')
        # if the string starts with these characters the data -->
        # --> is not added to the array
        if el.startswith('```') or el == "":
            continue
        data.append(el)
    except EOFError:
        break


for i in data:
    if i.startswith("#"):
        i = ''
        # clear the terminal
        print('\033c', end="")
        # delay before showing a new frame
        time.sleep(0.5)
    # text output using ansi code
    print(f"\033[32m {i}")
