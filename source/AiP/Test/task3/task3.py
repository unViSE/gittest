import blas_demo as blas
import random as rd
import time


"""
n =       1e5
n_stop =  1e6
step =    1e5
"""

# init var
alpha, pi2, k = 0, 180, 1
vx = vy = 0
type_vector = 0

try:
    type_vector = input("Enter the type:\n1-int\n2-float\n>>> ")
    if type_vector not in ['1', '2']:
        raise ValueError

    # the angle(alpha) between vectors can't be negative and can't be more than 180
    # Example: alpha = 181 is equal alpha = 1; alpha = 383 is equal alpha = 23
    alpha = float(input("Enter the angle between x and y (a > 0) >>> "))
    if alpha >= pi2:
        k = alpha // 180
        alpha -= pi2*k
    if alpha <= 0:
        raise ValueError

except ValueError:
    print("Enter the correct data!")
    exit(1)


# the length of vectors
n = 100000
n_stop = 1000000
step = 100000


# create a file(data_time.txt)
file = open("data_time.txt", "w")

print(f"\nAngle = {alpha}\n\n")
time.sleep(1)

while n <= n_stop:

    # random list from 0 to 49 numbers
    if type_vector == '1':
        vx = [rd.randint(0, 50) for j in range(n)]
        vy = [rd.randint(0, 50) for j in range(n)]
    elif type_vector == '2':
        vx = [rd.uniform(0, 50) for j in range(n)]
        vy = [rd.uniform(0, 50) for j in range(n)]

    # start time
    time_start = time.time()

    print(f"vector z = {blas.saxpy(n, alpha, vx, vy)}\n")

    # time stop
    time_stop = time.time()

    # calculates the different in time
    time_different = str(time_stop - time_start).replace(".", ',')

    # write to file
    file.write(f"{time_different}\n")

    n += step

file.close()