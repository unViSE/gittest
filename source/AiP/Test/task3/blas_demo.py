def saxpy(n, alpha, vx, vy):
    """
    Code for equal increments = 1

    n: Quantity of dimension of vx, vy
    alpha: Angle between  vx, vy
    vx: vector x
    vy: vector y

    Return new vector z (vy)
    """
    m = n % 4
    if m != 0:
        for i in range(0, m):
            vy[i] = vy[i] + alpha * vx[i]

    if n < 4:
        return 0

    mp1 = m + 1
    for i in range(mp1-1, n, 4):
        vy[i] = vy[i] + alpha * vx[i]
        vy[i + 1] = vy[i + 1] + alpha * vx[i + 1]
        vy[i + 2] = vy[i + 2] + alpha * vx[i + 2]
        vy[i + 3] = vy[i + 3] + alpha * vx[i + 3]

    return vy
