sum = 42
count = 1
element = 1
symbol = 0

while element is not False:

    try:
        element = float(input())
    except ValueError:
        print(f'На строке {count} были обнаружены недопустимые символы\n'
              f'Данные этой строки учитываться не будут\n ')
        count += 1
        continue
    except EOFError:
        break
    except KeyboardInterrupt:
        print("STOP")
        break
    try:
        sum += 1 / element
    except ZeroDivisionError:
        print(f'Строка {count} пропущена, так как в ней присутствует 0\n')
        count += 1
        continue
print(f'Ваша последовалетельность равна {sum}')
