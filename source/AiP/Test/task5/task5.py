from SoLE import cramer, gauss, random_matrix
import random as rd
import time
import memory_profiler as mp

print("Sample:")
for i in range(1, 4):
    for j in range(1, 2):
        print(f"(a{i}{j}) + x{j} + (a{i}{j+1}) + x{j+1} = (b{i})\n")


type_matrix = input(f"Type of Matrix? 1 - integer; 2 - float >>> ")

choice = input("Method? 1 - Cramer; 2 - Gauss-Jordan >>> ")

n = 1
n_stop = 9
step = 1

matrix = random_matrix(choice, type_matrix, n)


file = open("matrix_time.txt", 'w')

if choice == '1':
    while n <= n_stop:
        matrix = random_matrix(choice, type_matrix, n)
        answers = [rd.randint(0, 9) for i in range(n)]

        time_start = time.time()

        result = cramer(matrix, answers, n)

        time_stop = time.time()
        file.write(f"{str(time_stop - time_start).replace('.', ',')}\n")
        n += step


elif choice == '2':
    n = 100
    n_stop = 300
    step = 10
    while n <= n_stop:
        matrix = random_matrix(choice, type_matrix, n)

        time_start = time.time()

        result = gauss(matrix, n)

        time_stop = time.time()
        print(f"Memory usage in {n} matrix = {str(mp.memory_usage()[0] * 1024)} byte")
        file.write(f"{str(time_stop - time_start).replace('.', ',')}\n")
        n += step

file.close()
