import copy
import random as rd


# ---------------------Determinant------------------
def minor(matrix, i, j):
    c = matrix
    c = c[:i] + c[i+1:]
    for k in range(0, len(c)):
        c[k] = c[k][:j]+c[k][j+1:]
    return c


def det(matrix, n):
    if n == 1:
        return matrix[0][0]
    if n == 2:
        return matrix[0][0] * matrix[1][1] - matrix[0][1] * matrix[1][0]
    s = 0
    for i in range(0, n):
        m = minor(matrix, 0, i)
        s += ((-1)**i) * matrix[0][i] * det(m, n - 1)
    return s


# ---------------------Gauss-Jordan------------------
def gauss(matrix, n):
    for i in range(0, n):
        maximum_el = abs(matrix[i][i])
        maximum_row = i
        for j in range(i+1, n):
            if abs(matrix[j][i]) > maximum_el:
                maximum_el = abs(matrix[j][i])
                maximum_row = j

        for j in range(i, n+1):
            matrix[maximum_row][j], matrix[i][j] = matrix[i][j], matrix[maximum_row][j]

        for j in range(i+1, n):
            res = -matrix[j][i] / matrix[i][i]
            for k in range(i, n+1):
                if i == k:
                    matrix[j][k] = 0
                else:
                    matrix[j][k] += res * matrix[i][k]

    solve = [0 for el in range(n)]
    for i in range(n-1, -1, -1):
        solve[i] = matrix[i][n] / matrix[i][i]
        for j in range(i-1, -1, -1):
            matrix[j][n] -= matrix[j][i] * solve[i]
    return solve


# ---------------------Cramer------------------
def cramer(matrix, answers, n):
    result = []
    basic_det = det(matrix, n)
    if basic_det == 0:
        return False
    for number in range(len(answers)):
        matrix_copy = copy.deepcopy(matrix)
        for i in range(len(answers)):
            matrix_copy[i][number] = answers[i]
        result.append(det(matrix_copy, n) / basic_det)
    return result


# ---------------------Random Matrix------------------
def random_matrix(choice, type_, n):
    if type_ == '1':
        if choice == '2':
            return [[rd.randint(0, 9) for i in range(n+1)] for j in range(n)]
        return [[rd.randint(0, 9) for i in range(n)]for j in range(n)]

    if type_ == '2':
        if choice == '2':
            return [[rd.uniform(0, 9) for i in range(n+1)] for j in range(n)]
        return [[rd.uniform(0, 9) for i in range(n)]for j in range(n)]
