import random as rd


def mult_with_vector(a, b, n):
    c = [[0 for row in range(n)] for col in range(n)]
    for i in range(0, n, 2):
        for j in range(0, n, 2):
            s1 = s2 = s3 = s4 = 0
            for k in range(0, n):
                s1 = s1 + b[k][j] * a[i][k]
                s2 = s2 + b[k][j + 1] * a[i][k]
                s3 = s3 + b[k][j] * a[i + 1][k]
                s4 = s4 + b[k][j + 1] * a[i + 1][k]
            c[i][j] = s1
            c[i][j + 1] = s2
            c[i + 1][j] = s3
            c[i + 1][j + 1] = s4
    return c


n = 1000
A = [[rd.uniform(0, 10) for i in range(n)] for j in range(n)]
B = [[rd.uniform(0, 10) for i in range(n)] for j in range(n)]
print(mult_with_vector(A, B, n))
