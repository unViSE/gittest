import time
import matrix


type_matrix = input('Enter matrix type (int-1 or float-2) >>> ')
if type_matrix not in ['1', '2']:
    print("Enter int or float!")
    exit(1)


"""
size =      1000
size_stop = 101000
step =      10000
"""


size = 50
size_stop = 250
step = 20


file = open("data_time.txt", "w")

while size <= size_stop:

    print(f"\nCalculates for {size} dimension matrix:")

    for j in range(5):

        time_start = time.time()

        matrix1 = matrix.rand(size, type_matrix)
        matrix2 = matrix.rand(size, type_matrix)

        c = matrix.mult(matrix1, matrix2)

        time_stop = time.time()
        time_difference = str(time_stop - time_start).replace('.', ',')

        print(f"Try № {j + 1} = {time_difference} sec")
        file.write(f"{time_difference}\n")

    size += step


file.close()
