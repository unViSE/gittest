import random as rd


def mult(matrix1, matrix2):
    C = [[0 for e in range(len(matrix1))] for r in range(len(matrix2[0]))]

    for i in range(len(matrix1)):
        for j in range(len(matrix2[0])):
            for k in range(len(matrix2)):
                C[i][j] += matrix1[i][k]*matrix2[k][j]
    return C


def rand(size, type_matrix):
    c = 0
    if type_matrix == '1':
        c = [[rd.randint(0, 10) for el in range(size)] for j in range(size)]
    elif type_matrix == '2':
        c = [[rd.uniform(0, 10) for el in range(size)] for j in range(size)]
    return c
