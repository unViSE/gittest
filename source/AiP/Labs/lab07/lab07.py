import module7 as m7


def main():
    while True:

        try:
            x, x_stop = float(input("Initial boundary for x >>> ")), float(input("Final boundary for x >>> "))
            if x >= x_stop:
                raise ValueError

            step, a = float(input("Enter a step >>> ")), float(input('Enter A parameter >>> '))
            if step <= 0:
                raise ValueError

        except ValueError:
            print('Incorrect value\n')
            continue

        data = [[], [], []]

        data = m7.all_func(x, x_stop, step, a, data)

        # BLOCK_START:
        # open, write and close the data.txt
        f = open("data.txt", 'w')
        f.write(f"{m7.list_str(data[0])}\n{m7.list_str(data[1])}\n{m7.list_str(data[2])}\n")
        f.close()

        del data

        f = open("data.txt", 'r')

        g_lst, f_lst, y_lst = map(str, [f.readline() for m in range(3)])
        f.close()
        # BLOCK_STOP

        # convert from string to 1D array
        g_lst = m7.from_str_to_list(g_lst)
        f_lst = m7.from_str_to_list(f_lst)
        y_lst = m7.from_str_to_list(y_lst)

        # Output block of x and f(x) G, F, Y
        print("\n\t\t\tBlock of G")
        for e in range(0, len(g_lst), 2):
            print(f"The x = {g_lst[e]} \t\t G(x) = {g_lst[e+1]}")

        print("\n\t\t\tBlock of G")
        for e in range(0, len(f_lst), 2):
            print(f"The x = {f_lst[e]} \t\t G(x) = {f_lst[e + 1]}")

        print("\n\t\t\tBlock of G")
        for e in range(0, len(y_lst), 2):
            print(f"The x = {y_lst[e]} \t\t G(x) = {y_lst[e + 1]}")

        x_lst = g_lst[::2]
        y, y1, y2 = g_lst[1::2], f_lst[1::2], y_lst[1::2]

        del g_lst, f_lst, y_lst

        # print the Max and Min value of the function and its argument
        y, y2 = list(filter(None, y)), list(filter(None, y2))

        print(f"\nThe Max and Min value of the function argument is {max(x_lst):.5f} \t\t {min(x_lst):.5f}\n"
              f"The Max and Min value of the function G is {max(y):.5f} \t\t {min(y):.5f}\n"
              f"The Max and Min value of the function F is {max(y1):.5f} \t\t {min(y1):.5f}\n"
              f"The Max and Min value of the function Y is {max(y2):.5f} \t\t {min(y2):.5f}\n")

        print(f"The array of x = {m7.list_str(x_lst)}\n\n"
              f"The array of G(x) = {m7.list_str(y)}\n\n"
              f"The array of F(x) = {m7.list_str(y1)}\n\n"
              f"The array of Y(x) = {m7.list_str(y2)}\n")

        find_str = input(f"Which string do you want to find: ")
        print(f"\nCount in x = {m7.list_str(x_lst).count(find_str)}\n"
              f"Count in G(x) = {m7.list_str(y).count(find_str)}\n"
              f"Count in F(x) = {m7.list_str(y1).count(find_str)}\n"
              f"Count in Y(x) = {m7.list_str(y2).count(find_str)}\n")

        # exit
        rep = input("\nDo you wanna exit? [*] - Yes >>> ")
        if rep != "*":
            print("Ok, the program will repeat again\n")
            continue
        else:
            break


if __name__ == "__main__":
    main()
