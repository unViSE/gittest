import module3 as m3


def main():
    while True:
        print(f"\nEnter 1 - Calculate G\n"
              f"Enter 2 - Calculate F\n"
              f"Enter 3 - Calculate Y\n")


        try:
            value = int(input("Function name >>> "))
            if value not in [1, 2, 3]:
                raise ValueError

            x, x_stop = float(input("Initial boundary for x >>> ")), float(input("Final boundary for x >>> "))
            if x >= x_stop:
                raise ValueError

            step, a = float(input("Enter a step >>> ")), float(input('Enter A parameter >>> '))
            if step <= 0:
                raise ValueError

        except ValueError:
            print('Incorrect value\n')
            continue

        data = []

        # data is a list that stores a tuple of n number of elements
        data = m3.choice(value, x, x_stop, step, a, data)

        # print x and f(x)
        for e in data:
            print(f"The x = {e[0]} \t\t f(x) = {e[1]}")

        # change from "ND" list to "1D" list
        data = [j for sub in [list(el) for el in data] for j in sub]

        # splitting the list into 2 parts: x_plot(x), y_plot(f(x))
        x_plot, y_plot = data[::2], data[1::2]

        m3.plotting(value, x_plot, y_plot)

        # exit
        rep = input("\nDo you wanna exit? [*] - Yes >>> ")
        if rep != "*":
            print("Ok, the program will repeat again\n")
            continue
        else:
            break


if __name__ == "__main__":
    main()
