import math


# Блок ввода переменных
a = float(input('Введите x >>> '))
x = float(input('Введите a >>> '))

# Блок расчета функций G, F, Y
G = -16 * a ** 2 + 24 * a * x - 27 * x ** 2 / 45 * a ** 2 - 29 * a * x + 4 * x ** 2
F = -1 * math.atan(10 * a ** 2 + 13 * a * x - 30 * x ** 2)
Y = math.log(2 * a ** 2 + 19 * a * x + 9 * x ** 2 + 1) / math.log(10)

# Блок вывода функций G, F, Y
print(f'G = {complex(G)}, \t\t {G}, \t\t {G:.5f}')
print(f'F = {complex(F)}, \t\t {F}, \t\t {F:.5f}')
print(f'Y = {complex(Y)}, \t\t {Y}, \t\t {Y:.5f}')

