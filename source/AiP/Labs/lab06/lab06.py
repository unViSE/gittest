import module6 as m6


def main():
    while True:

        try:
            x, x_stop = float(input("Initial boundary for x >>> ")), float(input("Final boundary for x >>> "))
            if x >= x_stop:
                raise ValueError

            step, a = float(input("Enter a step >>> ")), float(input('Enter A parameter >>> '))
            if step <= 0:
                raise ValueError
            
        except ValueError:
            print('Incorrect value\n')
            continue

        data = [[], [], []]

        data = m6.all_func(x, x_stop, step, a, data)

        # output block x and f (x) of functions G, F, Y
        print("\n\t\t\tBlock of G")
        for e in data[0]:
            print(f"The x = {e[0]} \t\t G(x) = {e[1]}")

        print("\n\t\t\tBlock of F")
        for e in data[1]:
            print(f"The x = {e[0]} \t\t F(x) = {e[1]}")

        print("\n\t\t\tBlock of Y")
        for e in data[2]:
            print(f"The x = {e[0]} \t\t Y(x) = {e[1]}")

        g_lst, f_lst, y_lst = m6.arr_proc(data[0]), m6.arr_proc(data[1]), m6.arr_proc(data[2])
        x_lst = g_lst[::2]
        y, y1, y2 = g_lst[1::2], f_lst[1::2], y_lst[1::2]

        del g_lst, f_lst, y_lst

        # print the Max and Min value of the function and its argument
        y, y2 = list(filter(None, y)), list(filter(None, y2))
        print(f"\nThe Max and Min value of the function argument is ({max(x_lst):.5f}) \t\t ({min(x_lst):.5f})\n"
              f"The Max and Min value of the function G is ({max(y):.5f}) \t\t ({min(y):.5f})\n"
              f"The Max and Min value of the function F is ({max(y1):.5f}) \t\t ({min(y1):.5f})\n"
              f"The Max and Min value of the function Y is ({max(y2):.5f}) \t\t ({min(y2):.5f})\n")

        print(f"The array of x = {m6.list_str(x_lst)}\n\n"
              f"The array of G(x) = {m6.list_str(y)}\n\n"
              f"The array of F(x) = {m6.list_str(y1)}\n\n"
              f"The array of Y(x) = {m6.list_str(y2)}\n")

        find_str = input(f"Which string do you want to find: ")
        print(f"\nCount in x = {m6.list_str(x_lst).count(find_str)}\n"
              f"Count in G(x) = {m6.list_str(y).count(find_str)}\n"
              f"Count in F(x) = {m6.list_str(y1).count(find_str)}\n"
              f"Count in Y(x) = {m6.list_str(y2).count(find_str)}\n")


        # exit
        rep = input("\nDo you wanna exit? [*] - Yes >>> ")
        if rep != "*":
            print("Ok, the program will repeat again\n")
            continue
        else:
            break


if __name__ == "__main__":
    main()
