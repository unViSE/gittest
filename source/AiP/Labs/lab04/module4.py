import numpy as np
import matplotlib.pyplot as plt


# START: Block for calculating G, F, Y

def g_calc(x, a):
    """
    A function that calculate G

    Return x(float), g(float)
                     or
                     None
    """
    if 45 * a ** 2 - 29 * a * x + 4 * x ** 2 == 0:
        # Code to add point discontinuity of the function
        return x, None
    g = -16 * a ** 2 + 24 * a * x - 27 * x ** 2 / 45 * a ** 2 - 29 * a * x + 4 * x ** 2
    return x, g


def f_calc(x, a):
    """
    A function that calculate F

    Return s(float), f(float)
                     or
                     None
    """
    f = -1 * np.arctan(10 * a ** 2 + 13 * a * x - 30 * x ** 2)
    return x, f


def y_calc(x, a):
    """
    A function that calculate Y

    Return x(float), yl(float)
                     or
                     None
    """
    ynl = 2 * a ** 2 + 19 * a * x + 9 * x ** 2 + 1

    if ynl <= 0:
        # Code to add point discontinuity of the function
        return x, None

    yl = np.log(ynl) / np.log(10)
    return x, yl

# STOP: Block for calculating G, F, Y


def loop(func, x, x_stop, step, a, data):
    """
    A function that cyclically calculate the value of G or F or Y and its argument
    # "param: func" for selecting G or F or Y via choice(...)

    Return data([tuple, tuple, tuple ... tuple])
    """
    while x <= x_stop:
        data.append(func(x, a))
        x += step
    return data


def choice(value, x, x_stop, step, a, data):
    """
    A function that select G or F or Y

    Return loop(...) that
                    Return data([tuple, tuple, tuple ... tuple])
    """
    if value == 1:
        return loop(g_calc, x, x_stop, step, a, data)

    if value == 2:
        return loop(f_calc, x, x_stop, step, a, data)

    if value == 3:
        return loop(y_calc, x, x_stop, step, a, data)


def plotting(value, x, y):
    """
    A function that will build a graph from the values x, y

    Return None
    """
    plt.plot(x, y, 'k')
    plt.axis('tight')
    plt.xlabel('x')

    if value == 1:
        plt.legend('G(x)')
        plt.title('График функции G(x)')
        plt.ylabel('G(x)')

    elif value == 2:
        plt.legend('F(x)')
        plt.title('График функции F(x)')
        plt.ylabel('F(x)')

    elif value == 3:
        plt.legend('Y(x)')
        plt.title('График функции Y(x)')
        plt.ylabel('Y(x)')

    plt.show()
