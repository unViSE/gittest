import random as rd
import module8 as m8
import time


"""
n_start =        100,000 time ~100 - ~2000 msec
n_stop  =      1,000,000 
step =           100,000
"""


n = 100000
n_stop = 1000000
step = 100000

# Init var
des_point = radius = 0

while True:

    try:
        radius = float(input("Enter the radius >>> "))
    except ValueError:
        print(f'Enter correct data!\n')
        continue
    break


# create data_time.txt
fin = open("data_time.txt", "w")

# a loop that calculates the points affiliation and the time spent on it
while n <= n_stop:

    print(f"\nCalculate in n = {n} points\n\n"
          f"Point affiliation:")

    for j in range(5):

        time_start = time.time()

        # random points with x,y coordinates
        data = [[rd.uniform(-10, 10) for el in range(n)], [rd.uniform(-10, 10) for el in range(n)]]

        # Ox for circle
        random_point = rd.randint(0, n)
        x0, y0 = data[0][random_point], data[1][random_point]

        print(f"Try № {j+1} = {m8.counter_points(radius, x0, y0, data)}")

        time_stop = time.time()
        time_difference = str(time_stop - time_start).replace('.', ',')

        # data for Excel
        fin.write(f"{time_difference}\n")

    n += step


fin.close()
