def point_affiliation(radius, x0, y0, x1, y1):
    """
    A function that calculates whether a point belongs to a circle

    Return true if the point lies in or on the circle
    """
    d = radius**2 - (x1-x0)**2 - (y1-y0)**2
    return True if d >= 0 else False


def counter_points(radius, x0, y0, data):
    """
    A function that calculates the count of points inside and on circle

    Return the count of points that belong to the circle
    """
    count = 0
    for x1, y1 in zip(data[0], data[1]):
        if point_affiliation(radius, x0, y0, x1, y1):
            count += 1
    return count
