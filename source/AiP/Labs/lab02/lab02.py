import math


# Блок вывода возможностей программы
print('Введите 1 - вычислить функцию G')
print('Введите 2 - вычислить функцию F')
print('Введите 3 - вычислить функцию Y')

# Блок для отлова возможных ошибок
try:
    a, x = float(input('Введите x >>> ')), float(input('Введите a >>> '))
except ValueError:
    print('Пожалуйста введите число')
    exit(1)

try:
    number_of_values = int(input('Введите число для своей функции >>> '))
except ValueError:
    print('Пожалуйста введите номер своей функции.')
    exit(1)

# Блок каскадных условий для подсчета соответствующей функций
if number_of_values == 1:
    gd = 45 * a ** 2 - 29 * a * x + 4 * x ** 2

    try:
        G = -16 * a ** 2 + 24 * a * x - 27 * x ** 2 / gd
    except ZeroDivisionError:
        print('Знаменатель функции G равен 0')
        exit(1)

    print(f'G = {complex(G)}, \t\t {G}, \t\t {G:.5f}')

elif number_of_values == 2:
    F = -1 * math.atan(10 * a ** 2 + 13 * a * x - 30 * x ** 2)
    print(f'F = {complex(F)}, \t\t {F}, \t\t {F:.5f}')


elif number_of_values == 3:
    ynl = 2 * a ** 2 + 19 * a * x + 9 * x ** 2 + 1

    try:
        Y = math.log(ynl) / math.log(10)
    except ValueError:
        print('Нарушено основное логарифмическое правило')
        exit(1)

    print(f'Y = {complex(Y)}, \t\t {Y}, \t\t {Y:.5f}')
