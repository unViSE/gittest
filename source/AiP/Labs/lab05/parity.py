import module5 as m5


# a function that counts an even number of digits in number
print("The number you want to count the number of even numbers for\n")
try:
    number_parity = float(input("Number >>> "))
except ValueError:
    print("Enter the number!")
print(f"Count of even numbers = {m5.parity(number_parity)}")