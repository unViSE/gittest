import module5 as m5


def main():
    while True:
        print(f"\nEnter 1 - Calculate G\n"
              f"Enter 2 - Calculate F\n"
              f"Enter 3 - Calculate Y\n")

        try:
            value = int(input("Function name >>> "))
            if value not in [1, 2, 3]:
                raise ValueError

            x, x_stop = float(input("Initial boundary for x >>> ")), float(input("Final boundary for x >>> "))
            if x >= x_stop:
                raise ValueError

            step, a = float(input("Enter a step >>> ")), float(input('Enter A parameter >>> '))
            if step <= 0:
                raise ValueError

        except ValueError:
            print('Incorrect value\n')
            continue

        data = []

        # data is a list that stores a tuple of n number of elements
        data = m5.choice(value, x, x_stop, step, a, data)

        # print x and f(x)
        for e in data:
            print(f"The x = {e[0]} \t\t f(x) = {e[1]}")

        # change from "ND" list to "1D" list
        data = [j for sub in [list(el) for el in data] for j in sub]

        # splitting the list into 2 parts: x_plot(x), y_plot(f(x))
        x_plot, y_plot = data[::2], data[1::2]

        m5.plotting(value, x_plot, y_plot)

        # print the Max and Min value of the function and its argument
        x1 = x_plot
        y1 = list(filter(None, y_plot))
        print(f"\nThe Max and Min value of the function argument is ({max(x1):.5f}) \t\t ({min(x1):.5f})\n"
              f"The Max and Min value of the function is ({max(y1):.5f}) \t\t ({min(y1):.5f})\n")

        # print the arrays of x and f(x) ===> string
        print(f"The array of x = {', '.join([str(el) for el in x1])}\n")
        print(f"The array of f(x) = {', '.join([str(el) for el in y1])}\n")

        # find the required number of rows
        find_str = input(f"Which string do you want to find: ")
        print(f"\nCount in x = {', '.join([str(el) for el in x1]).count(find_str)}")
        print(f"Count in y = {', '.join([str(el) for el in y1]).count(find_str)}\n")


        # exit
        rep = input("\nDo you wanna exit? [*] - Yes >>> ")
        if rep != "*":
            print("Ok, the program will repeat again\n")
            continue
        else:
            break


if __name__ == "__main__":
    main()
