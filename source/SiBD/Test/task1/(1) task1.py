import random
import time
import collections as coll
import matplotlib.pyplot as plt
import matplotlib.ticker as tick


def sorted_array(array):
    """
    A function that calculates the number of digits from 0 to 9 in an array of random real numbers
    
    Return a dictionary
    """
    return coll.Counter([i for i in range(10) for j in range(1000) if i <= array[j] < i + 1])


def main():
    random_numb = [random.uniform(0, 10) for i in range(1000)]

    stop_time = time.time()

    count_dict = sorted_array(random_numb)

    print("--- %s seconds ---" % (time.time() - stop_time))

    data = [[], []]

    for i in count_dict:
        data[0].append(i), data[1].append(count_dict[i])

    fig, ax = plt.subplots()

    ax.bar(data[0], data[1], color='grey')
    ax.set_title("Number of points from 0 to 9")

    ax.xaxis.set_major_locator(tick.MultipleLocator(1))
    ax.yaxis.set_major_locator(tick.MultipleLocator(10))

    plt.show()


if __name__ == "__main__":
    main()
