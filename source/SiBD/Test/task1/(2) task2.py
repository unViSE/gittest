import random
import time
import matplotlib.pyplot as plt
import matplotlib.ticker as tick


random_numb = [random.uniform(0, 10) for i in range(1000)]

stop_time = time.time()

x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
random_numb = sorted(random_numb)
count = 0
count_numb = []

for i in range(10):
    for j in range(1000):
        if i <= random_numb[j] < i + 1:
            count += 1
    count_numb.append(count)
    count = 0


print("--- %s seconds ---" % (time.time() - stop_time))

fig, ax = plt.subplots()

ax.bar(x, count_numb, color='grey')
ax.set_title("Number of points from 0 to 9")

ax.xaxis.set_major_locator(tick.MultipleLocator(1))
ax.yaxis.set_major_locator(tick.MultipleLocator(10))

plt.show()
